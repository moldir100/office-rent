import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from "bootstrap-vue";
import Vuex from 'vuex'
import store from './vuex/store'
import JwPagination from 'jw-vue-pagination';
import config from '@/config'

import {  BootstrapVueIcons } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

Vue.component('jw-pagination', JwPagination);

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.prototype.$config = config
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Vuex);

new Vue({
  router,
  connection: config.socketUrl,
  render: h => h(App),
  store
}).$mount('#app')
