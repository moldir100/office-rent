import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        office:[],
        requests:[],
        fullOffices:{
            offices:[],
            buildings:[]
        }
    },
    actions: {
        async loadPosts ({ commit }) {
            try {
                const response = await axios.get('http://192.168.17.178:3001/office');
                commit('SET_ITEMS', response.data)
            }
            catch (error) {
                console.log(error);
            }
        },

        // update(id, data) {
        //     return http.put(`/tutorials/${room}`, data);
        // }


        updateOffice ({commit},room) {
            axios.put(`http://192.168.17.178:3001/office/${room}`, {
                admin: this.admin,
                area: this.area,
                address: this.address,
                room: this.room,
                level: this.level,
                options: this.options,
                status: this.status,
                cost: this.cost,
                bcID: this.bcID
            })
                .then(() => {
                    commit('UPDATE_OFFICE', room)
                })
        },
            // axios
            //     .put('http://127.0.0.1:5000/lamp/' + this.$route.params.id,
            //         {value: this.value},
            //         {headers: {
            //                 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            //             }
            //         })
        // },
        // async editOffice({})axios.put(`api/student/`+id, {
        //     first_name:this.first_name,
        //     last_name:this.last_name,
        //     student_number:this.student_number,
        //     phone_number:this.phone_number,
        //     email:this.email,
        //     birth_date:this.birth_date,
        //     school_name:this.school_name,
        // });

        // async loadAll ({ commit }) {
        //     try {
        //         const response = await axios.get('http://192.168.17.177:5001/office');
        //         commit('SET_ALL', response.data)
        //     }
        //     catch (error) {
        //         console.log(error);
        //     }
        // },

        async loadRequests ({ commit }) {
            try {
                const response = await axios.get('http://192.168.17.178:3001/request');
                commit('SET_REQUESTS', response.data)
            }
            catch (error) {
                console.log(error);
            }
        },

        async addOffice({commit}, new_office) {
            commit('addOffice', new_office)
            await axios.post(' http://192.168.17.178:3001/office', new_office)
        },

        async loadBuildings ({ commit }) {
            try {
                const response = await axios.get('http://192.168.17.178:3001/office');
                commit('SET_BUILDING', response.data)
            }
            catch (error) {
                console.log(error);
            }
        },
        //тест
        async deleteOffice ({commit}, room) {
            await axios.delete(`http://192.168.17.178:3001/office/${room}`)
            .then(() => {
                commit('DELETE_OFFICE', room)
            })
        },

        async deleteRequest ({commit}, id) {
            await axios.delete(`http://192.168.17.178:3001/request/${id}`)
                .then(() => {
                    commit('DELETE_REQUEST', id)
                })
        },
    },
    mutations: {
        SET_ITEMS (state, office) {
            state.office = office
        },
        SET_REQUESTS (state, requests) {
            state.requests = requests
        },
        SET_BUILDING (state, building) {
            state.building = building
        },

        //push new offices внутри офисы
        addOffice(state,new_office){
            state.office.push(new_office)
        },
        DELETE_OFFICE(state, room) {
            // let index = state.office.findIndex(o => office.id == id);
            // state.office.splice(index,1);
            let index = state.office.filter(o => o.room != room);
            state.office.splice(index,1);
            // state.office.pop(index);
        },

        UPDATE_OFFICE(state, room){
            let index = state.requests.filter(o => o.room != room);
            state.office = office
        },

        DELETE_REQUEST(state, id) {
            // let index = state.office.findIndex(o => office.id == id);
            // state.office.splice(index,1);
            let index = state.requests.filter(o => o.id != id);
            // state.office.pop(index);
            state.requests.splice(index,1);
        }


    },
    getters: {
        office: state => {
            return state.office;
        },
        requests: state => {
            return state.requests;
        },

        buildings: state => {
            return state.buildings;
        },

        fullBuildings(state) {
            const build = state.building.reduce((sum, cur) => {
                const curBuilding = cur;
                curBuilding.office = state.office.filter(
                    (office) => office.bc_id === cur.bc_id
                );
                sum.push(curBuilding);
                return sum;
            }, []);
            return build;
        },
    }
})

export default store