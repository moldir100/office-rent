import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/offices/Home.vue'
import bc from "@/views/buildings/bc.vue";
import clientOffice from "@/views/request/clientOffice";
import Office from "@/views/Office";
import requests from "@/views/request/requests";
import Edit from "@/views/Edit";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: Edit
  },
  {
    path: '/bc',
    name: 'bc',
    component: bc
  },
  {
    path: '/client',
    name: 'clientOffice',
    component: clientOffice
  },
  {
    path: '/office',
    name: 'Office',
    component: Office
  },
  {
    path: '/request',
    name: 'requests',
    component: requests
  },

  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  // },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
